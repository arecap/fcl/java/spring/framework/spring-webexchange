package org.springframework.web.exchange.config;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.springframework.beans.factory.annotation.Value;

public class InformationExchangeJwtProperties {

    @Value("${information.exchange.signature.key}")
    private String signatureKey;

    @Value("${information.exchange.signature.algorithm}")
    private String sa;

    @Value("${information.exchange.issuer}")
    private String issuer;

    @Value("${spring.application.name}")
    private String applicationName;

    public String getSignatureKey() {
        return signatureKey;
    }

    public SignatureAlgorithm getSa() {
        return SignatureAlgorithm.forName(sa);
    }

    public String getIssuer() {
        return issuer == null ? applicationName : issuer;
    }

    public boolean isSigned() {
        try {
            getSa();
        } catch (SignatureException se) {
            return false;
        }
        return true;
    }

}
