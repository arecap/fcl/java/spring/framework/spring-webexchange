package org.springframework.web.exchange.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.*;
import org.springframework.web.exchange.filter.InformationExchangeFilter;
import org.springframework.web.exchange.support.InformationExchangePostProcessor;

import static org.springframework.beans.factory.config.BeanDefinition.ROLE_SUPPORT;

@Configuration
@ComponentScan("org.springframework.web.servlet.exchange.")
@PropertySource("classpath:informationexchange.properties")
public class InformationExchangeConfigurer implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Bean
    @Role(ROLE_SUPPORT)
    public InformationExchangeJwtProperties getInformationExchangeJwtProperties() {
        return new InformationExchangeJwtProperties();
    }

    @Bean
    @Role(ROLE_SUPPORT)
    public InformationExchangeFilter getInformationExchangeFilter() {
        return new InformationExchangeFilter();
    }

    @Bean
    @Role(ROLE_SUPPORT)
    public InformationExchangePostProcessor getInformationExchangePostProcessor() {
        return new InformationExchangePostProcessor(getInformationExchangeJwtProperties(), applicationContext);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
