package org.springframework.web.exchange.support;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.Environment;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.exchange.InformationExchangeContext;
import org.springframework.web.exchange.annotation.InformationExchange;
import org.springframework.web.exchange.config.InformationExchangeJwtProperties;

import javax.servlet.http.HttpServletResponse;

public class InformationExchangePostProcessor implements BeanPostProcessor {

    /** Logger available to subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    private InformationExchangeJwtProperties jwtProperties;

    private ApplicationContext applicationContext;

    private ObjectMapper objectMapper = new ObjectMapper();

    public InformationExchangePostProcessor(InformationExchangeJwtProperties jwtProperties, ApplicationContext applicationContext) {
        this.jwtProperties = jwtProperties;
        this.applicationContext = applicationContext;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(bean != null) {
            InformationExchange informationExchangeInfo = AnnotationUtils.findAnnotation(bean.getClass(), InformationExchange.class);
            if(informationExchangeInfo != null) {
                ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
                if(servletRequestAttributes == null) {
                    logger.warn("Early initialisation of InformationExchange bean type [" + bean.getClass() + "]. RequestContext not initialized!");
                } else {
                    HttpServletResponse response = servletRequestAttributes.getResponse();
                    if(InformationExchangeContext.class.isAssignableFrom(response.getClass())) {
                        InformationExchangeContext informationExchangeContext = (InformationExchangeContext) response;
                        informationExchangeContext.refresh();
                        String beanJti = informationExchangeContext.getApplicationJti(beanName);
                        if(beanJti != null && beanJti.trim().length() > 0) {
                            Claims beanClaims = parseInformationExchangeClaims(informationExchangeInfo, beanJti);
                            return objectMapper.convertValue(beanClaims, bean.getClass());
                        }
                        logger.debug("Not jti store in InformationExchangeContext for beanName ["+beanName+"]");
                    } else {
                        logger.debug("ServletRequestAttributes response not wrapped!");
                    }
                }

            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    protected Claims parseInformationExchangeClaims(InformationExchange info, String jti) {
        Environment env = applicationContext.getEnvironment();
        JwtParser jwtParser = Jwts.parser();
        return info.algorithm().length() > 0 ?
                jwtParser.setSigningKey(env.resolvePlaceholders(info.key()).getBytes()).parseClaimsJws(jti).getBody() :
                jwtParser.parseClaimsJwt(jti).getBody();
    }

}
