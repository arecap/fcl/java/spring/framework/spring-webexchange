package org.springframework.web.exchange;

import io.jsonwebtoken.Claims;

public interface InformationExchangeContext extends Claims {

    String INFORMATION_EXCHANGE = "Information-Exchange";

    void refresh();

    void commit();

    String getApplicationJti(String subject);

    String getUrl();

}
