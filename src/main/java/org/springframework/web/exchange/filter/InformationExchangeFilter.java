package org.springframework.web.exchange.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultIntroductionAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.util.ClassUtils;
import org.springframework.web.exchange.config.InformationExchangeJwtProperties;
import org.springframework.web.exchange.support.InformationExchangeContextAdvice;
import org.springframework.web.filter.RequestContextFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class InformationExchangeFilter extends RequestContextFilter {

    /** Logger available to subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private InformationExchangeJwtProperties jwtProperties;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        InformationExchangeContextAdvice delegatingServletInterceptor =
                new InformationExchangeContextAdvice(jwtProperties, applicationContext, objectMapper);
        HttpServletResponse wrapResponse = wrapHttpResponse(response, delegatingServletInterceptor);
        super.doFilterInternal(request, wrapResponse, filterChain);
    }

    protected HttpServletResponse wrapHttpResponse(HttpServletResponse response, InformationExchangeContextAdvice delegatingServletInterceptor) {
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setTargetClass(response.getClass());
        proxyFactory.setTarget(response);
        proxyFactory
                .setInterfaces(ClassUtils
                        .getAllInterfacesForClass(
                                response.getClass(),
                                applicationContext.getClassLoader()));
        proxyFactory.addAdvisor(new DefaultIntroductionAdvisor(delegatingServletInterceptor));
        proxyFactory.setOptimize(true);
        return (HttpServletResponse) proxyFactory.getProxy();
    }

}
