package org.springframework.web.exchange.annotation;

import org.springframework.context.annotation.Import;
import org.springframework.web.exchange.config.InformationExchangeConfigurer;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(InformationExchangeConfigurer.class)
@Documented
public @interface EnableWebExchange {
}
