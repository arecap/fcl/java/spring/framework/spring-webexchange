package org.springframework.web.exchange.annotation;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

import static org.springframework.web.util.TagUtils.SCOPE_REQUEST;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
@Documented
public @interface InformationExchange {

    String algorithm() default "";

    String key() default "";

}
