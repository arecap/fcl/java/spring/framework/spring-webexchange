package org.springframework.web.servlet.exchange.test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.exchange.annotation.InformationExchange;

@InformationExchange
@JsonIgnoreProperties(ignoreUnknown = true)
public class DummyTestIe2  {

    @JsonProperty
    private String valueStr = "valueStr";

    @JsonProperty
    private int valueInt = 1;

    @JsonProperty
    private DummyTestIe1 child;

    public String getValueStr() {
        return valueStr;
    }

    public void setValueStr(String valueStr) {
        this.valueStr = valueStr;
    }

    public int getValueInt() {
        return valueInt;
    }

    public void setValueInt(int valueInt) {
        this.valueInt = valueInt;
    }

    public DummyTestIe1 getChild() {
        return child;
    }

    public void setChild(DummyTestIe1 child) {
        this.child = child;
    }
}
