package org.springframework.web.servlet.exchange.test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.exchange.annotation.InformationExchange;

@InformationExchange
@JsonIgnoreProperties(ignoreUnknown = true)
public class DummyTestIe1  {

    @JsonProperty
    private String valueStr = "valueStr";

    @JsonProperty
    private int valueInt = 1;

    public String getValueStr() {
        return valueStr;
    }

    public void setValueStr(String valueStr) {
        this.valueStr = valueStr;
    }
}
