/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.web.servlet.exchange.test;


import org.contextualj.lang.annotation.expression.SourceName;
import org.contextualj.lang.annotation.expression.SourceType;
import org.contextualj.lang.annotation.pointcut.Bid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.context.annotation.Scope;
import org.springframework.cop.annotation.ContextOriented;
import org.springframework.web.servlet.mvc.Controller;

import static org.springframework.web.util.TagUtils.SCOPE_REQUEST;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@ContextOriented
@SourceType(Controller.class)
@Scope(SCOPE_REQUEST)
@Role(BeanDefinition.ROLE_SUPPORT)
public class InformationExchangeDummyTest {


    @Autowired
    private DummyTestIe1 dummyTestIe1;

    @Autowired
    private DummyTestIe2 dummyTestIe2;

    @Bid
    @SourceName("handleRequest")
    public void beforeHandleRequest() {
        dummyTestIe2.setChild(dummyTestIe1);
    }

}
